import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import apiLink from '../api/apilink';

Vue.use(Vuex, axios);
export default new Vuex.Store({
  state: {
    beers: [],
    savedBeers: []
  },
  mutations: {
    SET_BEERS(state, beers) {
      state.beers = beers;
    }
  },
  actions: {
    loadBeers({ commit }) {
      axios
        .get(apiLink)
        .then((data: any) => {
          const beers = data.data;
          commit('SET_BEERS', beers);
        })
        .catch((error: any) => {
          const errorcontainer = error;
          errorcontainer();
        });
    },
    addBeer(context, beer) {
      context.commit('ADD_BEER', beer);
    }
  },
  getters: {
    getBeer(state) {
      return state.savedBeers;
    }
  }
});
